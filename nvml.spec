%global __provides_exclude_from ^%{_libdir}/pmdk_debug/.*\\.so.*$

%define min_libfabric_ver 1.4.2
%define min_ndctl_ver 60.1
%define upstreamversion 2.1.0

Name:		nvml
Summary:	Persistent Memory Development Kit (formerly NVML)
Version:	2.1.0
Release:	1
License:	BSD-3-Clause
URL:		http://pmem.io/pmdk

Source0:        https://github.com/pmem/pmdk/releases/download/%{upstreamversion}/pmdk-%{upstreamversion}.tar.gz

BuildRequires:	gcc make glibc-devel autoconf automake man doxygen pkgconfig cmake
BuildRequires:	ndctl-devel >= %{min_ndctl_ver} daxctl-devel >= %{min_ndctl_ver}
BuildRequires:	libfabric-devel >= %{min_libfabric_ver}

Requires: libfabric >= %{min_libfabric_ver} openssh-clients

ExclusiveArch: x86_64 loongarch64

Obsoletes: nvml-tools < %{version}-%{release}

Obsoletes: libpmemblk-debug < %{version}-%{release}
Obsoletes: libpmemblk-devel < %{version}-%{release}
Obsoletes: libpmemblk < %{version}-%{release}
Obsoletes: libpmem-debug < %{version}-%{release}
Obsoletes: libpmem-devel < %{version}-%{release}
Obsoletes: libpmemlog < %{version}-%{release}
Obsoletes: libpmem < %{version}-%{release}
Obsoletes: libpmemobj-devel < %{version}-%{release}
Obsoletes: libpmemobj < %{version}-%{release}
Obsoletes: libpmemlog-debug < %{version}-%{release}
Obsoletes: libpmemlog-devel < %{version}-%{release}
Obsoletes: daxio < %{version}-%{release}
Obsoletes: pmempool < %{version}-%{release}
Obsoletes: libpmempool < %{version}-%{release}
Obsoletes: libpmempool-devel < %{version}-%{release}
Obsoletes: libpmempool-debug < %{version}-%{release}
Obsoletes: libpmemobj-debug < %{version}-%{release}
Obsoletes: libpmem2 < %{version}-%{release}
Obsoletes: libpmem2-debug < %{version}-%{release}
Obsoletes: libpmem2-devel < %{version}-%{release}
Obsoletes: pmreorder < %{version}-%{release}

Provides: libpmemblk-debug = %{version}-%{release}
Provides: libpmemblk-devel = %{version}-%{release}
Provides: libpmemblk = %{version}-%{release}
Provides: libpmem-debug = %{version}-%{release}
Provides: libpmem-devel = %{version}-%{release}
Provides: libpmemlog = %{version}-%{release}
Provides: libpmem = %{version}-%{release}
Provides: libpmemobj-devel = %{version}-%{release}
Provides: libpmemobj = %{version}-%{release}
Provides: libpmemlog-debug = %{version}-%{release}
Provides: libpmemlog-devel = %{version}-%{release}
Provides: daxio = %{version}-%{release}
Provides: pmempool = %{version}-%{release}
Provides: libpmempool = %{version}-%{release}
Provides: libpmempool-devel = %{version}-%{release}
Provides: libpmempool-debug = %{version}-%{release}
Provides: libpmemobj-debug = %{version}-%{release}
Provides: libpmem2 = %{version}-%{release}
Provides: libpmem2-debug = %{version}-%{release}
Provides: libpmem2-devel = %{version}-%{release}
Provides: pmreorder = %{version}-%{release}

Patch0000: fix-gcc-check-uninitialized.patch
Patch0001: 0001-resolving-loongarch64-compilation-errors.patch

%description
The Persistent Memory Development Kit (PMDK), formerly known as NVML, is
a growing collection of libraries and tools. Tuned and validated on both
Linux and Windows, the libraries build on the DAX feature of those operating
systems (short for Direct Access) which allows applications to access persistent
memory as memory-mapped files

%package_help

%prep
%autosetup -p1 -n pmdk-%{upstreamversion}

%build
%define _lto_cflags %{nil}

CFLAGS="%{optflags}" \
LDFLAGS="%{?__global_ldflags}" \
make %{?_smp_mflags} NORPATH=1 

%install
make install DESTDIR=%{buildroot} \
	LIB_AR= \
	prefix=%{_prefix} \
	libdir=%{_libdir} \
	includedir=%{_includedir} \
	mandir=%{_mandir} \
	bindir=%{_bindir} \
	sysconfdir=%{_sysconfdir} \
	docdir=%{_docdir} 
    
mkdir -p %{buildroot}%{_datadir}/pmdk
cp utils/pmdk.magic %{buildroot}%{_datadir}/pmdk/

%check
echo "PMEM_FS_DIR=/tmp" > src/test/testconfig.sh
echo "PMEM_FS_DIR_FORCE_PMEM=1" >> src/test/testconfig.sh
#make check

%ldconfig_scriptlets

%files
%license LICENSE
%doc ChangeLog CONTRIBUTING.md README.md
%dir %{_datadir}/pmdk
%{_libdir}/libpmem.so.*
%{_datadir}/pmdk/pmdk.magic
%{_libdir}/libpmem.so
%{_libdir}/pkgconfig/libpmem.pc
%{_includedir}/libpmem.h
%dir %{_libdir}/pmdk_debug
%{_libdir}/pmdk_debug/libpmem.so
%{_libdir}/pmdk_debug/libpmem.so.*
%{_libdir}/libpmemobj.so.*
%{_libdir}/libpmemobj.so
%{_libdir}/libpmem2.so
%{_libdir}/libpmem2.so.*
%{_libdir}/pkgconfig/libpmemobj.pc
%{_libdir}/pkgconfig/libpmem2.pc
%{_includedir}/libpmemobj.h
%{_includedir}/libpmemobj/*.h
%{_includedir}/libpmem2.h
%{_includedir}/libpmem2/*.h
%{_libdir}/pmdk_debug/libpmemobj.so
%{_libdir}/pmdk_debug/libpmemobj.so.*
%{_libdir}/pmdk_debug/libpmem2.so
%{_libdir}/pmdk_debug/libpmem2.so.*
%{_libdir}/libpmempool.so.*
%{_libdir}/libpmempool.so
%{_libdir}/pkgconfig/libpmempool.pc
%{_includedir}/libpmempool.h
%{_libdir}/pmdk_debug/libpmempool.so
%{_libdir}/pmdk_debug/libpmempool.so.*
%{_bindir}/pmempool
%config(noreplace) %{_sysconfdir}/bash_completion.d/pmempool
%{_bindir}/daxio
%{_bindir}/pmreorder
%{_datadir}/*

%files help
%{_mandir}/man1/pmempool.1.gz
%{_mandir}/man1/pmempool-*.1.gz
%{_mandir}/man1/daxio.1.gz
%{_mandir}/man3/pmem_*.3.gz
%{_mandir}/man3/pmemobj_*.3.gz
%{_mandir}/man3/pobj_*.3.gz
%{_mandir}/man3/oid_*.3.gz
%{_mandir}/man3/toid*.3.gz
%{_mandir}/man3/direct_*.3.gz
%{_mandir}/man3/d_r*.3.gz
%{_mandir}/man3/tx_*.3.gz
%{_mandir}/man3/pmempool_*.3.gz
%{_mandir}/man5/poolset.5.gz
%{_mandir}/man7/libpmem.7.gz
%{_mandir}/man7/libpmemobj.7.gz
%{_mandir}/man7/libpmempool.7.gz
%{_mandir}/man1/pmreorder.1.gz
%{_mandir}/man3/pmem2_badblock_clear.3.gz
%{_mandir}/man3/pmem2_badblock_context_delete.3.gz
%{_mandir}/man3/pmem2_badblock_context_new.3.gz
%{_mandir}/man3/pmem2_badblock_next.3.gz
%{_mandir}/man3/pmem2_config_delete.3.gz
%{_mandir}/man3/pmem2_config_new.3.gz
%{_mandir}/man3/pmem2_config_set_length.3.gz
%{_mandir}/man3/pmem2_config_set_offset.3.gz
%{_mandir}/man3/pmem2_config_set_protection.3.gz
%{_mandir}/man3/pmem2_config_set_required_store_granularity.3.gz
%{_mandir}/man3/pmem2_config_set_sharing.3.gz
%{_mandir}/man3/pmem2_config_set_vm_reservation.3.gz
%{_mandir}/man3/pmem2_deep_flush.3.gz
%{_mandir}/man3/pmem2_errormsg.3.gz
%{_mandir}/man3/pmem2_get_drain_fn.3.gz
%{_mandir}/man3/pmem2_get_flush_fn.3.gz
%{_mandir}/man3/pmem2_get_memcpy_fn.3.gz
%{_mandir}/man3/pmem2_get_memmove_fn.3.gz
%{_mandir}/man3/pmem2_get_memset_fn.3.gz
%{_mandir}/man3/pmem2_get_persist_fn.3.gz
%{_mandir}/man3/pmem2_map_delete.3.gz
%{_mandir}/man3/pmem2_map_from_existing.3.gz
%{_mandir}/man3/pmem2_map_get_address.3.gz
%{_mandir}/man3/pmem2_map_get_size.3.gz
%{_mandir}/man3/pmem2_map_get_store_granularity.3.gz
%{_mandir}/man3/pmem2_map_new.3.gz
%{_mandir}/man3/pmem2_perror.3.gz
%{_mandir}/man3/pmem2_source_alignment.3.gz
%{_mandir}/man3/pmem2_source_delete.3.gz
%{_mandir}/man3/pmem2_source_device_id.3.gz
%{_mandir}/man3/pmem2_source_device_usc.3.gz
%{_mandir}/man3/pmem2_source_from_anon.3.gz
%{_mandir}/man3/pmem2_source_from_fd.3.gz
%{_mandir}/man3/pmem2_source_get_fd.3.gz
%{_mandir}/man3/pmem2_source_numa_node.3.gz
%{_mandir}/man3/pmem2_source_pread_mcsafe.3.gz
%{_mandir}/man3/pmem2_source_pwrite_mcsafe.3.gz
%{_mandir}/man3/pmem2_source_size.3.gz
%{_mandir}/man3/pmem2_vm_reservation_delete.3.gz
%{_mandir}/man3/pmem2_vm_reservation_extend.3.gz
%{_mandir}/man3/pmem2_vm_reservation_get_address.3.gz
%{_mandir}/man3/pmem2_vm_reservation_get_size.3.gz
%{_mandir}/man3/pmem2_vm_reservation_map_find.3.gz
%{_mandir}/man3/pmem2_vm_reservation_map_find_first.3.gz
%{_mandir}/man3/pmem2_vm_reservation_map_find_last.3.gz
%{_mandir}/man3/pmem2_vm_reservation_map_find_next.3.gz
%{_mandir}/man3/pmem2_vm_reservation_map_find_prev.3.gz
%{_mandir}/man3/pmem2_vm_reservation_new.3.gz
%{_mandir}/man3/pmem2_vm_reservation_shrink.3.gz
%{_mandir}/man5/pmem_ctl.5.gz
%{_mandir}/man7/libpmem2.7.gz
%{_mandir}/man7/libpmem2_unsafe_shutdown.7.gz

%if 0%{?__debug_package} == 0
%debug_package
%endif

%changelog
* Thu Nov 28 2024 zhang_wenyu <zhang_wenyu@hoperun.com> - 2.1.0-1
- update to 2.1.0

* Tue Jan 23 2024 doupengda <doupengda@loongson.cn> - 1.13.1-2
- resolving loongarch64 compilation errors

* Tue Sep 5 2023 liyanan <thistleslyn@163.com> - 1.13.1-1
- update to 1.13.1

* Mon Jan 16 2023 dan <fzhang@zhixundn.com> 1.12.1-1
- update to 1.12.1

* Wed Jan 11 2023 Ge Wang<wangge20@h-partners.com> - 1.4.2-7
- fix build failure

* Wed Jul 21 2021 wangyue<wangyue92@huawei.com> - 1.4.2-6
- Remove gdb from Buildrequires

* Mon Jan 25 2021 lingsheng<lingsheng@huawei.com> - 1.4.2-5
- Initialize child_idx to fix warning

* Wed Aug 19 2020 lingsheng<lingsheng@huawei.com> - 1.4.2-4
- Disable check

* Tue Jul 28 2020 wutao<wutao61@huawei.com> - 1.4.2-3
- fix build error problems and delete extra tests

* Fri Dec 6 2019 caomeng<caomeng5@huawei.com> - 1.4.2-2
- Package init
